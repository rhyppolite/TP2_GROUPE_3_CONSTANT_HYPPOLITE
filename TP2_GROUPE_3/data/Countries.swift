//
//  Countries.swift
//  TP2_GROUPE_3
//
//  Created by mbds on 09/04/2021.
//

import Foundation


let countries = [
    Country(continent: "Europe", isoCode: "at", name: "Austria"),
    Country(continent: "Europe", isoCode: "be", name: "Belgium"),
    Country(continent: "Europe", isoCode: "de", name: "Germany"),
   // Country(continent: "Europe", isoCode: "el", name: "Greece"),
    Country(continent: "Europe", isoCode: "fr", name: "France"),
    //new pays -- Amérique
    Country(continent: "Amérique", isoCode: "ht", name: "Haiti"),
    Country(continent: "Amérique", isoCode: "jm", name: "Jamaique"),
    //Country(continent: "Amérique", isoCode: "tr", name: "Trinidad"),
    //Country(continent: "Amérique", isoCode: "br", name: "Brésil"),
    ///Afrique
    Country(continent: "Afrique", isoCode: "ni", name: "Nigéria"),
    Country(continent: "Afrique", isoCode: "se", name: "Sénégal"),
    Country(continent: "Afrique", isoCode: "ca", name: "Cameroun"),
    Country(continent: "Afrique", isoCode: "ci", name: "Cote d'Ivoire"),
    //Asie
    Country(continent: "Asie", isoCode: "ch", name: "Chine"),
    Country(continent: "Asie", isoCode: "ja", name: "Japon"),
    //Océanie
    Country(continent: "Océanie", isoCode: "nz", name: "Nouvelle Zélande")
]
