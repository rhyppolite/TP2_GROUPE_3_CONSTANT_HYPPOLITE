//
//  Country.swift
//  TP2_GROUPE_3
//
//  Created by mbds on 09/04/2021.
//

import Foundation


struct Country {
    var continent: String
    var isoCode: String
    var name: String
}
